#Zadatak 1.4.5 Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva SMSSpamCollection.txt [1]. 
#Ova datoteka sadrži 5574 SMS poruka pri cemu su neke oznacene kao spam, a neke kao ham.

#Primjer dijela datoteke:
#ham Yup next stop.
#ham Ok lar... Joking wif u oni...
#spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!

#a) Izracunajte koliki je prosjecan broj rijeci u SMS porukama koje su tipa ham, a koliko je  prosjecan broj rijeci u porukama koje su tipa spam.

ham_count = 0
ham_messages = 0
spam_count = 0
spam_messages = 0
spam_exclamation = 0
with open('SMSSpamCollection.txt') as f:
    line = f.readline().strip().split()
    while line:
        if line[0] == "ham":
            ham_count += 1
            ham_messages += len(line) - 1
        elif line[0] == "spam":
            spam_count += 1
            spam_messages += len(line) - 1
            if line[-1].endswith('!'):
                spam_exclamation += 1
        line = f.readline().strip().split()
print(f"Number of ham messages: {ham_count}, average lenght {ham_messages/ham_count}")
print(f"Number of spam messages: {spam_count}, average lenght {spam_messages/spam_count}")

#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?

print(f"Spam messages ending with '!': {spam_exclamation}")
