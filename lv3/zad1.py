#Zadatak 3.4.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
#Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljedeca pitanja:

import pandas as pd
import numpy as np
data = pd.read_csv('data_C02_emission.csv')

#a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip category.
print("a:")
print(len(data))
print(data.dtypes)
data.dropna(axis =0)  #brisanje izostalih vrijednosti
data.drop_duplicates() #brisanje dupliciranih vrijednosti
cols = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data[cols] = data[cols].astype('category')
print(data.dtypes)

#b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
#ime proizvodaca, model vozila i kolika je gradska potrošnja.
print("b:")
data=data.sort_values(by=['Fuel Consumption City (L/100km)'])
print("Top 3 lowest fuel consumers in cities")
print(data[['Make', 'Model','Fuel Consumption City (L/100km)']].head(3))
print("Top 3 highest fuel consumers in cities")
print(data[['Make', 'Model','Fuel Consumption City (L/100km)']].tail(3))

#c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija plinova za ova vozila?
print("c:")
a=(data['Engine Size (L)'] >=2.5) & (data['Engine Size (L)'] <=3.5)
print(f'Broj vozila velicine motora izmedu 2.5 i 3.5 L: {a.sum()}')
print(f'Prosjecne CO2 emisije tih vozila: {data["CO2 Emissions (g/km)"][a].mean()}')

#d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
#plinova automobila proizvodaca Audi koji imaju 4 cilindara?
print("d:")
b = (data['Make'] == 'Audi')
print(f'Broj Audi automobila u datasetu: {b.sum()}')
b = (data['Make'] == 'Audi') & (data['Cylinders'] == 4)
print(f'Prosjecne CO2 emisije Audija s 4 cilindra: {data["CO2 Emissions (g/km)"][b].mean()}')

#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na broj cilindara?
print("e:")
three = (data['Cylinders'] == 3)
print(f'Broj automobila s 3 cilindra: {three.sum()}')
print(f'Prosjecne CO2 emisije automobila s 3 cilindra: {data["CO2 Emissions (g/km)"][three].mean()}')

four = (data['Cylinders'] == 4)
print(f'Broj automobila s 4 cilindra: {four.sum()}')
print(f'Prosjecne CO2 emisije automobila s 4 cilindra: {data["CO2 Emissions (g/km)"][four].mean()}')

five = (data['Cylinders'] == 5)
print(f'Broj automobila s 5 cilindara: {five.sum()}')
print(f'Prosjecne CO2 emisije automobila s 5 cilindara: {data["CO2 Emissions (g/km)"][five].mean()}')

six = (data['Cylinders'] == 6)
print(f'Broj automobila s 6 cilindara: {six.sum()}')
print(f'Prosjecne CO2 emisije automobila s 6 cilindara: {data["CO2 Emissions (g/km)"][six].mean()}')

eight= (data['Cylinders'] == 8)
print(f'Broj automobila s 8 cilindara: {eight.sum()}')
print(f'Prosjecne CO2 emisije automobila s 8 cilindara: {data["CO2 Emissions (g/km)"][eight].mean()}')

ten = (data['Cylinders'] == 10)
print(f'Broj automobila s 10 cilindara: {ten.sum()}')
print(f'Prosjecne CO2 emisije automobila s 10 cilindara: {data["CO2 Emissions (g/km)"][ten].mean()}')

twelve = (data['Cylinders'] == 12)
print(f'Broj automobila s 12 cilindara: {twelve.sum()}')
print(f'Prosjecne CO2 emisije automobila s 12 cilindara: {data["CO2 Emissions (g/km)"][twelve].mean()}')

sixteen = (data['Cylinders'] == 16)
print(f'Broj automobila s 16 cilindara: {sixteen.sum()}')
print(f'Prosjecne CO2 emisije automobila s 16 cilindara: {data["CO2 Emissions (g/km)"][sixteen].mean()}')

#f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
print("f:")
diesel_cars = (data['Fuel Type']== 'D')
print(f'Prosjecna gradska potrosnja automobila koji koriste dizel je: {data["Fuel Consumption City (L/100km)"][diesel_cars].mean()}(L/100km), a medijalna vrijednost {data["Fuel Consumption City (L/100km)"][diesel_cars].median()}(L/100km)')
gasoline_cars = (data['Fuel Type']== 'X')
print(f'Prosjecna gradska potrosnja automobila koji koriste regularni benzin je: {data["Fuel Consumption City (L/100km)"][gasoline_cars].mean()}(L/100km), a medijalna vrijednost {data["Fuel Consumption City (L/100km)"][gasoline_cars].mean()}(L/100km)')

#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
print("g:")
data2 = (data['Cylinders']==4) & (data['Fuel Type']== 'D')
index = data['Fuel Consumption City (L/100km)'][data2].idxmax()
print(f'Automobil s 4 cilindra s dizelskim motorom s najvecom gradskom potrosnjom je: {data.loc[index]["Model"]}')

#h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
print("h:")
manuals = (data['Transmission'].str.contains('^M'))
print(f'Broj automobila s rucnim mjenjacem je: {manuals.sum()}')

#i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
print("i:")
print("Korelacije numerickih velicina:")
print(data.corr(numeric_only=True))
#sto je veci iznos korelacije veca je povezanost vrijednosti dviju velicina