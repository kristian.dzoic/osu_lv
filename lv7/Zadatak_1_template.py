import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering

def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
#plt.figure()
#plt.scatter(X[:,0],X[:,1])
#plt.xlabel('$x_1$')
#plt.ylabel('$x_2$')
#plt.title('podatkovni primjeri')
#plt.show()

#Zadatak 7.5.1 Skripta zadatak_1.py sadrži funkciju generate_data koja služi za generiranje umjetnih podatkovnih primjera kako bi se demonstriralo grupiranje. Funkcija prima cijeli broj
#koji definira željeni broju uzoraka u skupu i cijeli broj od 1 do 5 koji definira na koji nacin ce se generirati podaci, a vraca generirani skup podataka u obliku numpy polja pri cemu su prvi i
#drugi stupac vrijednosti prve odnosno druge ulazne velicine za svaki podatak. Skripta generira 500 podatkovnih primjera i prikazuje ih u obliku dijagrama raspršenja.

#1. Pokrenite skriptu. Prepoznajete li koliko ima grupa u generiranim podacima? Mijenjajte nacin generiranja podataka.

#Pokretanjem skripte s parametrima (500,1) generira podatke razvrstane u 3 razlicite grupe.
#Koristenjem parametra flagc==2 grupe podataka su transformirane pomocu dot funkcije.
#Koristenjem parametra flagc==3 generiraju se 4 grupe podataka od kojih su 2 manje, a 2 vise rasprsene oko svog sredista.
#Koristenjem parametra flagc==4 generiraju se podatci grupirani u dvije koncentricne kruznice.
#Koristenjem parametra flagc==4 generiraju se podatci grupirani u dva luka obrnute orijentacije

#2. Primijenite metodu K srednjih vrijednosti te ponovo prikažite primjere, ali svaki primjer obojite ovisno o njegovoj pripadnosti pojedinoj grupi. 
#Nekoliko puta pokrenite programski kod. Mijenjate broj K. Što primjecujete? 
km = KMeans ( n_clusters =3 , init ='random',n_init =5 , random_state =0 )
km.fit(X)
labels = km.predict( X )

LABEL_COLOR_MAP = {0 : 'r',
                   1 : 'g',
                   2 : 'b',
                   3 : 'y'
                   }
label_color = [LABEL_COLOR_MAP[l] for l in labels]

plt.figure()
plt.scatter(X[:,0],X[:,1],c=label_color)
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#3. Mijenjajte nacin definiranja umjetnih primjera te promatrajte rezultate grupiranja podataka (koristite optimalni broj grupa). Kako komentirate dobivene rezultate?
X = generate_data(500, 5)
km = KMeans ( n_clusters =2 , init ='random',n_init =5 , random_state =0 )
km.fit(X)
labels = km.predict( X )

LABEL_COLOR_MAP = {0 : 'r',
                   1 : 'g',
                   2 : 'b',
                   3 : 'y'
                   }
label_color = [LABEL_COLOR_MAP[l] for l in labels]

plt.figure()
plt.scatter(X[:,0],X[:,1],c=label_color)
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#Kod generiranja clustera gdje su 2 clustera vise, a 2 manje rasprsena algoritam ce pogresno pridjeliti neke elemente vise rasprenog skupa u onaj manje rasprseni.
#Kod generiranja koncentricnih kruznica te lukova obrnute orijentacije algoritam nece rasporediti podatke u grupe po tim oblicima nego ce ih grupirati na lijevu odnosno desnu stranu.