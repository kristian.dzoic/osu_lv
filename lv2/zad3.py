#Zadatak 2.4.3 Skripta zadatak_3.py ucitava sliku ’road.jpg’. Manipulacijom odgovarajuce
#numpy matrice pokušajte:

import numpy as np
import matplotlib.pyplot as plt

#a) posvijetliti sliku,
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
brightness = 150 
for x in range(len(img)):
    for y in range(len(img[x])):
        if(img[x, y] < 255 - brightness):
            img[x, y] += brightness
        else:
            img[x, y] = 255
plt.figure ()
plt.imshow(img, cmap= "gray")
plt.show()

#b) prikazati samo drugu cetvrtinu slike po širini, 
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
start = int(len(img[0]) * 0.25)
end = int(len(img[0]) * 0.5)
img = img[:,start:end]
plt.imshow(img, cmap="gray")
plt.show()

#c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
plt.imshow(np.rot90(np.rot90(np.rot90(img))), cmap = "gray")
plt.show()

#d) zrcaliti sliku.
img = plt.imread("road.jpg")
img = img[:,:,0].copy()
img[:,:] = img[:,::-1]
plt.imshow(img, cmap = "gray")
plt.show()