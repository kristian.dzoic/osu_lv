#Zadatak 2.4.2 Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja data pri cemu je u 
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
#stupac polja je masa u kg.

import numpy as np
import matplotlib.pyplot as plt

#a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
data=np.loadtxt('lv2\data.csv', delimiter=',',skiprows=1)
data=data.transpose()
num_rows, num_cols = data.shape
print(f"Mjerenja su vrsena nad {num_cols} osoba")

#b) Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter.
height=data[1]
weight=data[2]
plt.scatter(height, weight)
plt.xlabel ('Visina')
plt.ylabel ('Masa')
plt.title ('Odnos visine i mase')
plt.show()

#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
height=data[1][0:-1:50]
weight=data[2][0:-1:50]
plt.scatter(height, weight)
plt.xlabel ('Visina')
plt.ylabel ('Masa')
plt.title ('Odnos visine i mase svake pedesete osobe iz mjerenja')
plt.show()


#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom podatkovnom skupu.
height=data[1]
print(f"Minimalna visina u podatkovnom skupu je {height.min()}")
print(f"Maksimalna visina u podatkovnom skupu je {height.max()}")
print(f"Srednja visina u podatkovnom skupu je {height.mean()}")

#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
ind=data[0]
print(ind)
male_height = np.empty((0,1), float)
female_height = np.empty((0,1), float)

for index, item in enumerate(ind):
    if item==1.0:
        male_height = np.append(male_height, np.array([[data[1][index]]]), axis=0)
    else:
        female_height = np.append(female_height, np.array([[data[1][index]]]), axis=0)
print(f"Minimalna visina zena u podatkovnom skupu je {female_height.min()}")
print(f"Maksimalna visina zena u podatkovnom skupu je {female_height.max()}")
print(f"Srednja visina zena u podatkovnom skupu je {female_height.mean()}")
print(f"Minimalna visina muskaraca u podatkovnom skupu je {male_height.min()}")
print(f"Maksimalna visina muskaraca u podatkovnom skupu je {male_height.max()}")
print(f"Srednja visina muskaraca u podatkovnom skupu je {male_height.mean()}")