#Zadatak 8.4.2 Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1 i MNIST skup podataka. 
#Pomocu matplotlib biblioteke potrebno je prikazati nekoliko loše klasificiranih slika iz
#skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvidenu mrežom.

import numpy as np
from tensorflow import keras as keras
from keras import layers
from matplotlib import pyplot as plt
from keras.models import load_model

num_classes = 10
input_shape = (28, 28, 1)

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)
x_train_s = x_train_s.reshape(60000,784)
x_test_s = x_test_s.reshape(10000,784)

model = load_model ('lv8/FCN/')
model.summary()
predictions = model.predict (x_test_s)
print(predictions.argmax(axis=1))
print(y_test_s.argmax(axis=1))

for i in range (10000):
    if predictions.argmax(axis=1)[i] != y_test_s.argmax(axis=1)[i]:
        print(i, predictions.argmax(axis=1)[i], y_test_s.argmax(axis=1)[i])
        plt.figure()
        plt.title('Predvidena oznaka: ' + str(predictions.argmax(axis=1)[i]) + ' Stvarna oznaka: ' + str(y_test_s.argmax(axis=1)[i]))
        plt.imshow(x_test[i])
        plt.show()
