#Zadatak 2.4.4 Napišite program koji ce kreirati sliku koja sadrži  cetiri kvadrata crne odnosno 
#bijele boje (vidi primjer slike 2.4 ispod). Za kreiranje ove funkcije koristite numpy funkcije
#zeros i ones kako biste kreirali crna i bijela polja dimenzija 50x50 piksela. Kako biste ih složili
#u odgovarajuci oblik koristite numpy funkcije  hstack i vstack.

import numpy as np
import matplotlib.pyplot as plt

light = np.ones((50, 50))
dark = np.zeros((50, 50))
first_column = np.vstack((dark, light))
second_column = np.vstack((light, dark))
img = np.hstack((first_column, second_column))
plt.imshow(img, cmap= "gray")
plt.show()
