#Zadatak 1.4.4 Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva song.txt.
#Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u 
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci. 
#Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

dict = {}
file = open("song.txt")
for line in file:
    for word in line.split():
        if word in dict:
            dict[word] = dict[word] + 1
        else:
            dict.update({word : 1})
print(dict)
one_time_words = 0
for key in dict.keys():
    if dict[key] == 1:
        print(key)
        one_time_words += 1
print(one_time_words)
