import numpy as np
import tensorflow as tf
keras = tf.keras
layers = tf.keras.layers
cifar10 = tf.keras.datasets.cifar10
from matplotlib import pyplot as plt


# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = tf.keras.utils.to_categorical(y_train)
y_test = tf.keras.utils.to_categorical(y_test)

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
#model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
#model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
    keras.callbacks.TensorBoard(log_dir = 'logs/cnn_dropout',
                                update_freq = 100),
    keras.callbacks.EarlyStopping ( monitor ="val_loss",
                                        patience = 5,
                                        verbose = 1),
]

model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')

#Zadatak 9.4.1 Skripta Zadatak_1.py ucitava CIFAR-10 skup podataka. Ovaj skup sadrži 50000 slika u skupu za ucenje i 10000 slika za testiranje.
#Slike su RGB i rezolucije su 32x32. Svakoj slici je pridružena jedna od 10 klasa ovisno koji je objekt prikazan na slici. Potrebno je:

#1. Proucite dostupni kod. Od kojih se slojeva sastoji CNN mreža? Koliko ima parametara mreža?

#CNN mreza sastoji se od 3 konvolucijska sloja, 3 sloja sazimanja, sloja ravnanja te 2 potpuno povezana sloja od 500 i 10 neurona.
#Mreza ima 1,122,758 parametara

#2. Pokrenite ucenje mreže. Pratite proces ucenja pomocu alata Tensorboard na sljedeci nacin.
#Pokrenite Tensorboard u terminalu pomocu naredbe:
#tensorboard –logdir=logs
#i zatim otvorite adresu http://localhost:6006/ pomocu web preglednika.

#3. Proucite krivulje koje prikazuju tocnost klasifikacije i prosjecnu vrijednost funkcije gubitka na skupu podataka za ucenje i skupu podataka za validaciju.
#Što se dogodilo tijekom ucenja mreže? Zapišite tocnost koju ste postigli na skupu podataka za testiranje.

#Tijekom ucenja mreze tocnost na trening skupu je rasla do 15-20 epohe, a poslije toga nije bilo znacajnih promjena, tocnost na podatcima za testiranje je svoj maksimum dosegla 
#oko 15. epohe nakon cega je malo padala i rasla oko iste vrijednosti
#Gubitak na podatcima za treniranje je konstantno padao dok je na podatcima za testiranje minimum dosegao u 6. epohi te nakon toga kontinuirano krenuo rasti
#Tocnost na testnom skupu podataka: 71.33


#Zadatak 9.4.2 Modificirajte skriptu iz prethodnog zadatka na nacin da na odgovarajuca mjesta u
#mrežu dodate droput slojeve. Prije pokretanja ucenja promijenite Tensorboard funkciju povratnog
#poziva na nacin da informacije zapisuje u novi direktorij (npr. =/log/cnn_droput). Pratite tijek
#ucenja. Kako komentirate utjecaj dropout slojeva na performanse mreže?

#Tocnost na testnom skupu podataka: 74.14
#Tocnost na testnom skupu je povecana u odnosu na CNN bez dropout sloja, a tocnost trening skupa je nesto manja nego na proslom primjeru.

#Zadatak 9.4.3 Dodajte funkciju povratnog poziva za rano zaustavljanje koja ce zaustaviti proces
#ucenja nakon što se 5 uzastopnih epoha ne smanji prosjecna vrijednost funkcije gubitka na validacijskom skupu.
 
#Epoch 11: early stopping
#Tocnost na testnom skupu podataka: 75.44

#Zadatak 9.4.4 Što se dogada s procesom ucenja:
#1. ako se koristi jako velika ili jako mala velicina serije?
# Ako se koristi jako velika velicina serije brzina ucenja se povecava, ali se tocnost smanjuje, kod jako male velicine serije obrnuto
#2. ako koristite jako malu ili jako veliku vrijednost stope ucenja?

#3. ako izbacite odredene slojeve iz mreže kako biste dobili manju mrežu?
#4. ako za 50% smanjite velicinu skupa za ucenje?