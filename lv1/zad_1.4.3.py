#Zadatak 1.4.3 Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji 
#sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovarajucu poruku.

list =[]
while True:
    print("Enter a number: ")
    x = input()
    if x == "Done":
        break
    else:
        try:    
            list.append(int(x))
        except:
            print("Please enter a number")

print("Input list",list)
print (f"You entered {len(list)} numbers")
print (f"Average of the list is {float(sum(list))/float(len(list))}")
print (f"Minimum value is {min(list)}")
print (f"Maximum value is {max(list)}")
list.sort()
print("Sorted list:",list)