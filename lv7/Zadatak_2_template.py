import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("lv7/imgs/test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

#Zadatak 7.5.2 Kvantizacija boje je proces smanjivanja broja razlicitih boja u digitalnoj slici, ali uzimajuci u obzir da rezultantna slika vizualno bude što slicnija originalnoj slici.
#Jednostavan nacin kvantizacije boje može se postici primjenom algoritma K srednjih vrijednosti na RGB vrijednosti elemenata originalne slike.
#Kvantizacija se tada postiže zamjenom vrijednosti svakog elementa originalne slike s njemu najbližim centrom. Na slici 7.3a dan je primjer originalne
#slike koja sadrži ukupno 106,276 boja, dok je na slici 7.3b prikazana rezultantna slika nakon
#kvantizacije i koja sadrži samo 5 boja koje su odredene algoritmom K srednjih vrijednosti.

#1. Otvorite skriptu zadatak_2.py. Ova skripta ucitava originalnu RGB sliku test_1.jpgte ju transformira u podatkovni skup koji dimenzijama 
# odgovara izrazu (7.2) pri cemu je n broj elemenata slike, a m je jednak 3. Koliko je razlicitih boja prisutno u ovoj slici?

#2. Primijenite algoritam K srednjih vrijednosti koji ce pronaci grupe u RGB vrijednostima elemenata originalne slike.
kmeans = KMeans(n_clusters=5, random_state=0)
kmeans.fit(img_array_aprox)
labels = kmeans.predict(img_array_aprox)

#3. Vrijednost svakog elementa slike originalne slike zamijeni s njemu pripadajucim centrom.
def recreate_image(codebook, labels, w, h):
    return codebook[labels].reshape(w, h, -1)

plt.figure()
plt.clf()
plt.axis("off")
plt.title(f"Quantized image ({5} colors, K-Means)")
plt.imshow(recreate_image(kmeans.cluster_centers_, labels, w, h))
plt.show()

#4. Usporedite dobivenu sliku s originalnom. Mijenjate broj grupa K. Komentirajte dobivene rezultate.
#Dobivena slika je znacajno manje kvalitete medutim za potrebe lakog prepoznavanje teksta i brojeva je dovoljne kvalitete.
#Povecanjem broja K ne poboljsava se raspoznavanje simbola na slici

#5. Primijenite postupak i na ostale dostupne slike
def img_quant(filename) :
    img = Image.imread(filename)
    plt.figure()
    plt.title("Originalna slika")
    plt.imshow(img)
    plt.tight_layout()
    plt.show()
    img = img.astype(np.float64) / 255
    w,h,d = img.shape
    img_array = np.reshape(img, (w*h, d))
    img_array_aprox = img_array.copy()
    kmeans = KMeans(n_clusters=5, random_state=0)
    kmeans.fit(img_array_aprox)
    labels = kmeans.predict(img_array_aprox)
    plt.figure()
    plt.clf()
    plt.axis("off")
    plt.title(f"Quantized image ({5} colors, K-Means)")
    plt.imshow(recreate_image(kmeans.cluster_centers_, labels, w, h))
    plt.show()

img_quant("lv7/imgs/test_2.jpg")
img_quant("lv7/imgs/test_3.jpg")
img_quant("lv7/imgs/test_4.jpg")
img_quant("lv7/imgs/test_5.jpg")
img_quant("lv7/imgs/test_6.jpg")

#6. Graficki prikažite ovisnost J o broju grupa K. Koristite atribut inertia objekta klase KMeans. Možete li uociti lakat koji upucuje na optimalni broj grupa?


#7. Elemente slike koji pripadaju jednoj grupi prikažite kao zasebnu binarnu sliku. Što primjecujete?