#Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz  data_C02_emission.csv.
#Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih numerickih ulaznih velicina. 
#Detalje oko ovog podatkovnog skupa mogu se pronaci u 3. laboratorijskoj vježbi.

from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
import sklearn.metrics as met
import math

#a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.
data = pd.read_csv('lv4/data_C02_emission.csv', header = 0)
input = ['Fuel Consumption City (L/100km)',
         'Fuel Consumption Hwy (L/100km)',
         'Fuel Consumption Comb (L/100km)',
         'Fuel Consumption Comb (mpg)',
         'Cylinders',
         'Engine Size (L)']

output = ['CO2 Emissions (g/km)']
X= data[input].to_numpy()
y= data[output].to_numpy()
X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

#b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova
#o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom
plt.scatter(X_train[:,0],y_train,c='blue',s=1)
plt.scatter(X_test[:,0],y_test,c='red',s=1)
plt.xlabel ('Fuel Consumption City (L/100km)')
plt.ylabel ('CO2 Emissions (g/km)')
plt.show()

#c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti
#jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja 
#transformirajte ulazne velicine skupa podataka za testiranje.
sc = MinMaxScaler()
X_train_n = sc.fit_transform ( X_train )
X_test_n = sc.transform ( X_test )
fig, axs = plt.subplots(2)
fig.suptitle('Histogram vrijednosti jedne ulazne velicine prije i nakon skaliranja')
axs[0].hist(X_train[:,0],bins=5)
axs[1].hist(X_train_n[:,0],bins=5)
plt.show()

#d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i povežite ih s izrazom 4.6.
linearModel = lm.LinearRegression()
linearModel.fit( X_train_n , y_train )
print(linearModel.coef_)

#e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite pomocu 
#dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene dobivene modelom.
y_test_p = linearModel.predict ( X_test_n )
plt.scatter(X_test_n[:,0],y_test_p,c='red',s=1)
plt.scatter(X_test_n[:,0],y_test,c='blue',s=1)
plt.xlabel ('Fuel Consumption City (L/100km)')
plt.ylabel ('CO2 Emissions (g/km)')
plt.title ('Odnos stvarnih (plava boja) i procjenjenih(crvena boja) izlaznih velicina')
plt.show()

#f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na skupu podataka za testiranje.
MSE = met.mean_squared_error ( y_test , y_test_p )
print(f'The mean squared error is {MSE}')

RMSE = math.sqrt(MSE)
print(f'The root mean squared error is {RMSE}')

MAE = met.mean_absolute_error ( y_test , y_test_p )
print(f'The mean apsolute error is {MAE}')

MAPE = met.mean_absolute_percentage_error( y_test , y_test_p )
print(f'The mean apsolute percentage error is {MAPE}')

R2 = met.r2_score( y_test , y_test_p )
print(f'The determination cofficient is {R2}')

#g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj ulaznih velicina?
#Veci broj ulaznih velicina daje vecu tocnost pa su metrike pogreske manje, a manji broj ulaznih velicina smanjuje tocnost modela