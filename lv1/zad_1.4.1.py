#Zadatak 1.4.1 Napišite program koji od korisnika zahtijeva unos radnih sati te koliko je placen 
#po radnom satu. Koristite ugradenu Python metodu  input(). Nakon toga izracunajte koliko 
#je korisnik zaradio i ispišite na ekran. Na kraju prepravite rješenje na nacin da ukupni iznos 
#izracunavate u zasebnoj funkciji naziva  total_euro.

def total_euro(h,x):
    return float(x) * float(h)

print ('Enter work hours: ')
h = input()
print ('Enter pay per hour: ')
x = input()
p = total_euro(h,x)
print('You made ',p,' euros')