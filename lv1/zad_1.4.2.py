#Zadatak 1.4.2 Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
#nekakvu ocjenu i nalazi se izmedu 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju 
#sljedecih uvjeta: 
#>= 0.9 A
#>= 0.8 B
#>= 0.7 C
#>= 0.6 D
#< 0.6 F
#Ako korisnik nije utipkao broj, ispišite na ekran poruku o grešci (koristite try i except naredbe).
#Takoder, ako je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovarajucu poruku.

points = -1
while points < 0 or points > 1:
    try:
        points = float(input("Enter your points (0.0-1.0): "))
        if points < 0 or points > 1:
            print("Invalid points. Please enter a value between 0.0 and 1.0.")
    except ValueError:
        print("Invalid input. Please enter a number.")
if points < 0.6:
    print("Your grade is an F")
elif points < 0.7:
    print("Your grade is a D")
elif points < 0.8:
    print("Your grade is a C")
elif points < 0.9:
    print("Your grade is a B")
else:
    print("Your grade is an A")