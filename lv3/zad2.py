# Zadatak 3.4.2 Napišite programski kod koji ce prikazati sljedece vizualizacije:

import pandas as pd 
import matplotlib.pyplot as plt
data = pd.read_csv('osu_lv/lv3/data_C02_emission.csv')

#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind="hist", bins=20)
plt.show()

#b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu
#velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.
data['Fuel Type'] = data['Fuel Type'].astype('category')
colors = {'Z': 'darkgreen', 'X': 'green', 'E': 'yellow', 'D': 'black'}

data.plot.scatter(x="Fuel Consumption City (L/100km)", y="CO2 Emissions (g/km)", c=data["Fuel Type"].map(colors), s=50)
plt.show()

#c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tip
#goriva. Primjecujete li grubu mjernu pogrešku u podacima?
data.boxplot(column='CO2 Emissions (g/km)', by='Fuel Type')
plt.show()

#d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby.
grouped = data.groupby('Fuel Type').size()
grouped.plot(kind ='bar', xlabel='Fuel Type', ylabel='Vehicles', title='Number of vehicles of fuel type')
plt.show()

#e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na broj cilindara.
cylinder_grouped = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
cylinder_grouped.plot(kind='bar', x=cylinder_grouped.index, y=cylinder_grouped.values, xlabel='Cylinders', ylabel='CO2 emissions (g/km)', title='CO2 emissions by number of cylinders')
plt.show()