#Zadatak 2.4.1 Pomocu funkcija  numpy.array i matplotlib.pyplot pokušajte nacrtati sliku
#2.3 u okviru skripte zadatak_1.py. Igrajte se sa slikom, promijenite boju linija, debljinu linije i
#sl.

import numpy as np
import matplotlib.pyplot as plt

shape = [(1,1),(3,1),(3,2),(2,2)]
for points in [shape]:
    plt.plot(*zip(*(points+points[:1])), marker='o')
plt.axis ([0,4,0,4])
plt.xlabel ('x os')
plt.ylabel ('y os')
plt.title ('Primjer')
plt.show ()