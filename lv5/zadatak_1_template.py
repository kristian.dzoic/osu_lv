#Zadatak 5.5.1 Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
#ulazne velicine. Podaci su podijeljeni na skup za ucenje i skup za testiranje modela.

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score,precision_score,recall_score
from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a) Prikažite podatke za ucenje u x1−x2 ravnini matplotlib biblioteke pri cemu podatke obojite
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je moguce definirati boju svake klase.
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=matplotlib.colors.ListedColormap(['red', 'blue']),marker='x')
plt.show()

#b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa podataka za ucenje.
LogRegression_model = LogisticRegression()
LogRegression_model.fit (X_train,y_train)

#c) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke
#naucenog modela u ravnini x1 −x2 zajedno s podacima za ucenje. Napomena: granica
#odluke u ravnini x1−x2 definirana je kao krivulja: θ0+θ1x1+θ2x2 = 0.
x = np.linspace(-4, 4, 100)
y = -(LogRegression_model.coef_[0][0] * x + LogRegression_model.intercept_[0]) / LogRegression_model.coef_[0][1]
plt.plot(x, y)
plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=matplotlib.colors.ListedColormap(['red', 'blue']),marker='x')
plt.show()

#d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izracunate tocnost,
#preciznost i odziv na skupu podataka za testiranje.
y_test_p = LogRegression_model.predict(X_test)

cm = confusion_matrix (y_test,y_test_p)
print("Matrica zabune: ",cm)
disp = ConfusionMatrixDisplay(confusion_matrix (y_test,y_test_p))
disp.plot()
plt.show()
print("Tocnost: ", accuracy_score(y_test, y_test_p))
print("Preciznost: ", precision_score(y_test, y_test_p))
print("Odziv: ", recall_score(y_test, y_test_p))

#e) Prikažite skup za testiranje u ravnini x1−x2. Zelenom bojom oznacite dobro klasificirane
#primjere dok pogrešno klasificirane primjere oznacite crnom bojom.
y_wrong = y_test != y_test_p
plt.scatter(X_test[:,0], X_test[:,1], c = y_test_p, cmap=matplotlib.colors.ListedColormap(['green', 'black']))
plt.scatter(X_test[:,0], X_test[:,1], c = y_wrong, cmap=matplotlib.colors.ListedColormap(['green', 'black']))
plt.show()
