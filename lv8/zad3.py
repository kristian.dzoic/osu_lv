#Zadatak 8.4.3 Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1. Nadalje, skripta
#treba ucitati sliku test.png sa diska. Dodajte u skriptu kod koji ce prilagoditi sliku za mrežu,
#klasificirati sliku pomocu izgradene mreže te ispisati rezultat u terminal. Promijenite sliku
#pomocu nekog grafickog alata (npr. pomocu Windows Paint-a nacrtajte broj 2) i ponovo pokrenite
#skriptu. Komentirajte dobivene rezultate za razliˇcite napisane znamenke.

import numpy as np
from tensorflow import keras as keras
from keras import layers
from matplotlib import pyplot as plt
from keras.models import load_model

model = load_model ('lv8/FCN/')
model.summary()